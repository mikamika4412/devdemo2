<?php

namespace Mika\HelloWorld\Api;

use Mika\HelloWorld\Api\Data\ItemInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ItemRepositoryInterface
{
    /**
     * @param int $id
     * @return ItemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(int $id): ItemInterface;

    /**
     * @return Mika\HelloWorld\Api\Data\ItemInterface[];
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null): ItemSearchResultInterface;

    /**
     * @param ItemInterface $item
     * @return ItemInterface
     */
    public function save(ItemInterface $item): ItemInterface;

    /**
     * @param ItemInterface $selNews
     * @return bool
     */
    public function delete(ItemInterface $selNews): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool;

}
