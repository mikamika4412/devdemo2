<?php

namespace Mika\HelloWorld\Api;

interface ItemSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * @return \Mika\HelloWorrld\Api\Data\ItemInterface[]
     */
    public function getItems();

    /**
     * @param \Mika\HelloWorrld\Api\Data\ItemInterface[]
     * @return void
     */
    public function setItems(array $items);
}
