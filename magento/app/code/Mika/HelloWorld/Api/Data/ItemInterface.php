<?php

namespace Mika\HelloWorld\Api\Data;

interface ItemInterface
{
    const ID = 'news_id';
    const NEWS = 'news';

    /**
     * @return string
     */
    public function getNews(): string;

    /**
     * @param string $news
     * @return void
     */
    public function setNews(string $news): void;

    /**
     * @return string|null
     */
    public function getComment(): ?string;

    /**
     * @return void
     */
    public function setComment(int $string): void;

}
