<?php

namespace Mika\HelloWorld\Model;

use Mika\HelloWorld\Api\ItemSearchResultInterface;
use Magento\Framework\Api\SearchResults;

class IndexSearchResult extends SearchResults implements ItemSearchResultInterface
{
}
