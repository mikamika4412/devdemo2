<?php

namespace Mika\HelloWorld\Model;

use Mika\HelloWorld\Api\ItemSearchResultInterface;
use Mika\HelloWorld\Api\Data\ItemInterface;
use Mika\HelloWorld\Api\ItemRepositoryInterface;
use Mika\HelloWorld\Model\ResourceModel\Item\CollectionFactory;
use Mika\HelloWorld\Model\ResourceModel\Item as ItemResource;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Mika\HelloWorld\Api\ItemSearchResultInterfaceFactory;
use Mika\HelloWorld\Model\ItemFactory;

class ItemRepository implements \Mika\HelloWorld\Api\ItemRepositoryInterface
{
    private CollectionFactory $collectionFactory;
    private ItemResource $itemResource;
    private ItemFactory $itemFactory;
    private ItemSearchResultInterfaceFactory $searchResultInterfaceFactory;
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @param \Mika\HelloWorld\Model\ResourceModel\Item\CollectionFactory $collectionFactory
     * @param \Mika\HelloWorld\Model\ItemFactory                          $itemFactory
     * @param \Mika\HelloWorld\Model\ResourceModel\Item                   $itemResource
     * @param \Mika\HelloWorld\Api\ItemSearchResultInterfaceFactory       $searchResultInterfaceFactory
     * @param \Magento\Framework\Api\SearchCriteriaBuilder                $searchCriteriaBuilder
     */
    public function __construct(
        ItemFactory $itemFactory,
        CollectionFactory $collectionFactory,
        ItemResource $itemResource,
        ItemSearchResultInterfaceFactory $searchResultInterfaceFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->itemFactory = $itemFactory;
        $this->itemResource = $itemResource;
        $this->searchResultFactory = $searchResultInterfaceFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }
    /**
     * @param int $id
     * @return \Mika\HelloWorld\Api\Data\ItemInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id): ItemInterface
    {
        $object = $this->itemFactory->create();
        $this->itemResource->load($object, $id);
        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Unable to find entity with ID "%1"',
                $id));
        }
        return $object;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface|null $searchCriteria
     * @return \Mika\HelloWorld\Api\ItemSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null):ItemSearchResultInterface
    {
//        return $this->collectionFactory->create()->getItems();

        $collection = $this->collectionFactory->create();
        $searchCriteria = $this->searchCriteriaBuilder->create();

        if (null === $searchCriteria) {
            $searchCriteria = $this->searchCriteriaBuilder->create();
        } else {
            foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
                foreach ($filterGroup->getFilters() as $filter) {
                    $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                    $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
                }
            }
        }

        $searchResult = $this->searchResultFactory->create();
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);
        return $searchResult;
    }


    /**
     * @param \Mika\HelloWorld\Api\Data\ItemInterface $item
     * @return \Mika\HelloWorld\Api\Data\ItemInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(ItemInterface $item): ItemInterface
    {
        $this->itemResource->save($item);
        return $item;
    }

    /**
     * @param \Mika\HelloWorld\Api\Data\ItemInterface $selNews
     * @return bool
     */
    public function delete(ItemInterface $selNews): bool
    {
        try {
            $this->itemResource->delete($selNews);
        } catch (\Exception $e) {
            throw new StateException(__('Unable to remove entity #%1', $selNews->getId()));
        }
        return true;
    }
    /**
     * @param int $id
     * @return bool
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }




}
